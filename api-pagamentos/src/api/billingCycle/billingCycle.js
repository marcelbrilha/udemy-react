const restful = require('node-restful')

const mongoose = restful.mongoose

const creditSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'informe o nome do crédito']
  },
  value: {
    type: Number,
    min: 0,
    required: [true, 'informe o valor do crédito']
  }
})

const dbtSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'informe o nome do débito']
  },
  value: {
    type: Number,
    min: 0,
    required: [true, 'informe o valor do débito']
  },
  status: {
    type: String,
    required: false,
    uppercase: true,
    enum: ['PAGO', 'PENDENTE', 'AGENDADO']
  }
})

const billingCycleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'informe o nome do faturamento']
  },
  month: {
    type: Number,
    min: 1,
    max: 12,
    required: [true, 'informe o mês do faturamento']
  },
  year: {
    type: Number,
    min: 1970,
    max: 2100,
    required: [true, 'informe o ano do faturamento'],
    credits: [creditSchema],
    dbts: [dbtSchema]
  }
})

module.exports = restful.model('BillingCycle', billingCycleSchema)
